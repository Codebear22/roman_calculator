<?php


class RomanCalculatorTest extends PHPUnit_Framework_TestCase
{
    public function testRoman_mathReturnRoman(){

        $roman = new \Controller\RomanCalculator();

        $expression = "XX + VIII";

        $expected = "XXVIII";

        $this->assertEquals($expected, $roman->roman_math($expression));
    }

    public function testInt_to_roman(){

        $roman = new \Controller\RomanCalculator();

        $expression = 36;

        $expected = "XXXVI";

        $this->assertEquals($expected, $roman->int_to_roman($expression));
    }


    public function testRoman_to_int(){

        $roman = new \Controller\RomanCalculator();

        $expression = "XXVIII";

        $expected = 28;

        $this->assertEquals($expected, $roman->roman_to_int($expression));
    }

}