<?php

require_once 'vendor/autoload.php';
$roman =  new \Controller\RomanCalculator();
?>

<html>
<head>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<div class="container">
<h1>
    Roman Calculator
</h1>

<form class="form-inline"  method="post" action=<?php echo $_SERVER["PHP_SELF"]?>>
    <div class="form-group">
    <label for="expression">
        Enter Roman Expression <small class="text-muted">(eg. XX + XV)</small>
    </label>
    <input class="form-control" id="expression" name="expression">
    </div>
    <button class="btn btn-info" type="submit">Submit</button>
</form>
<p>
    <?php
    echo "<h3>Result</h3>";
if(isset($_POST["expression"])){
    $roman_expression = $_POST["expression"];
    $result = $roman->roman_math($roman_expression);
    echo $result;
    }
    ?></p>

</div>
</body>