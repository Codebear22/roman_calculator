<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/6/17
 * Time: 5:55 PM
 */
//echo romanToInt("XXVII");
//echo romanic_number(27);

namespace Controller;

class RomanCalculator{

    public $romans = array(
        'M' => 1000,
        'CM' => 900,
        'D' => 500,
        'CD' => 400,
        'C' => 100,
        'XC' => 90,
        'L' => 50,
        'XL' => 40,
        'X' => 10,
        'IX' => 9,
        'V' => 5,
        'IV' => 4,
        'I' => 1,
    );

    public function roman_math($roman_expression){
        $math_expression = explode(" ",$roman_expression);
        $operand1 = $this->roman_to_int($math_expression[0]);
        $operand2 = $this->roman_to_int($math_expression[2]);
        $operator = $math_expression[1];

        $result = $operator=='+' ? $operand1+$operand2 : ($operator=='-' ? $operand1-$operand2 : ($operator=='*' ? $operand1*$operand2 : $operand1/$operand2));

        return $this->int_to_roman($result);

    }


    public function roman_to_int($roman){


        $result = 0;

        foreach ($this->romans as $key => $value) {
            while (strpos($roman, $key) === 0) {
                $result += $value;
                $roman = substr($roman, strlen($key));
            }
        }
        return $result;
    }



    public function int_to_roman($integer, $upcase = true)
    {
        $return = '';
        while($integer > 0)
        {
            foreach($this->romans as $rom=>$arb)
            {
                if($integer >= $arb)
                {
                    $integer -= $arb;
                    $return .= $rom;
                    break;
                }
            }
        }

        return $return;
    }


}

